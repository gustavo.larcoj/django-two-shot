from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateForms, CategoryForm, AccountForm
@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    receipts.purchaser = request.user
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipts.html", context)
@login_required
def my_receipt_list(request):
    receipt= Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts/receipts.html", context)
@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = CreateForms(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect('home')
    else:
        form = CreateForms()
    context = {
        "form": form,
    }
    return render(request, 'receipts/create_receipts.html', context)
@login_required
def category_list(request):
    list= ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": list,
    }
    return render(request, "receipts/category_list.html", context)
@login_required
def account_list(request):
    receipt= Account.objects.filter(owner=request.user)
    context = {
        "account_list": receipt,
    }
    return render(request, "receipts/account_list.html", context)
@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_categories.html", context)
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
